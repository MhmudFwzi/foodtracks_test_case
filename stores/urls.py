from django.urls import path
from .views import (
    StoresApiView,
    StoreAPIView
)

urlpatterns = [
    path('store/', StoreAPIView.as_view()),
    path('stores-list', StoresApiView.as_view()),
    path('store', StoreAPIView.as_view()),
]