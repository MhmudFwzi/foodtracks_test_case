from django.db import models
from datetime import time

class Store(models.Model):
    store_name = models.CharField(max_length=150)
    store_address = models.CharField(max_length=500)
    store_opening_time = models.TimeField(default=time(8,0,0))
    store_closure_time = models.TimeField(default=time(20,0,0))

    def __str__(self):
        return self.store_name