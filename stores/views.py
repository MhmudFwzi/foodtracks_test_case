from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from django.core.paginator import Paginator
from .models import Store
from .serializers import StoreSerializer

class StoresApiView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        page_number = request.query_params.get('page_number', 1)
        page_size = request.query_params.get('page_size', 10)
        print(page_size)
        stores = Store.objects.all()
        paginator = Paginator(stores , page_size)
        serializer = StoreSerializer(paginator.page(page_number), many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)
    
class StoreAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        try:
            requested_id = request.query_params.get('store_id')
            store = Store.objects.get(id=requested_id)
            serializer = StoreSerializer(store)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except:
            return Response(None, status=status.HTTP_404_NOT_FOUND)

    
    def post(self, request, *args, **kwargs):
        data = {
            'store_name': request.data.get('store_name'),
            'store_address': request.data.get('store_address', ""), 
            'store_opening_time': request.data.get('store_opening_time'), 
            'store_closure_time': request.data.get('store_closure_time'), 
        }
        serializer = StoreSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def put(self, request, *args, **kwargs):
        to_be_updated_id = request.query_params.get('store_id')
        store = Store.objects.get(id=to_be_updated_id)
        data = {
            'store_name': request.data.get('store_name'), 
        }
        serializer = StoreSerializer(store ,data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    

    def delete(self, request, *args, **kwargs):
        to_be_deleted_id = request.query_params.get('store_id')
        try:
            to_be_deleted_store = Store.objects.get(id=to_be_deleted_id)
            to_be_deleted_store.delete()
            return Response(status=status.HTTP_200_OK)
        except:
            return Response(None, status=status.HTTP_404_NOT_FOUND)
