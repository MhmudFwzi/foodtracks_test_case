from rest_framework import serializers
from .models import Store

class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = ["store_name", "store_address", "store_opening_time", "store_closure_time"]