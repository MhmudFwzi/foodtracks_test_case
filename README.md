# foodtracks_test_case

If the project is already in an existing python3 virtualenv first install django by running

    $ pip install django
    $ pip3 install django_rest_framework
    
      
### No virtualenv

This assumes that `python3` is linked to valid installation of python 3 and that `pip` is installed and `pip3`is valid
for installing python 3 packages.

Installing inside virtualenv is recommended, however you can start your project without virtualenv too.

If you don't have django installed for python 3 then run:

    $ pip3 install django
    $ pip3 install django_rest_framework
    

# Stores
# Getting Started
    
Simply apply the migrations:

    $ python3 manage.py migrate
    

You can now run the development server:

    $ python3 manage.py runserver